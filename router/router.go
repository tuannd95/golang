package router

import (
	"github.com/at-tuannguyen3/live-me-framework/config/sql"
	"github.com/at-tuannguyen3/live-me-framework/router"
	"github.com/at-tuannguyen3/live-me-framework/utils"
	"github.com/digital-fortress/live-me/app/user"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

// Router struct
type Router struct {
	Mux           *chi.Mux
	SQLHandler    *sql.SQL
	LoggerHandler *utils.Logger
}

// InitializeRouter initializes Mux and middleware
func (r *Router) InitializeRouter() {

	r.Mux.Use(middleware.RequestID)
	r.Mux.Use(middleware.RealIP)
}

// SetupHandler set database and redis and usecase.
func (r *Router) SetupHandler() {
	requestRouter := router.ROUTER{
		MUX:           r.Mux,
		SQLHANDLER:    r.SQLHandler,
		LOGGERHANDLER: r.LoggerHandler,
	}
	bh, bu, br := router.Setup(requestRouter)
	user := user.NewHTTPHandler(bh, bu, br, r.SQLHandler)
	r.Mux.Route("/v1", func(cr chi.Router) {
		cr.Get("/users", user.GetUsers)
	})
}
