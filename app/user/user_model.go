package user

import (
	"github.com/at-tuannguyen3/live-me-framework/model"
)

// ModelUserStruct struct
type ModelUserStruct struct {
	ID       uint64 `gorm:"column:id;not null;primary_key"`
	Name     string `gorm:"column:name;not null"`
	Email    string `gorm:"column:email"`
	Password string `gorm:"column:password"`
	model.BaseModel
}

// TableName for ModelUserStruct struct
func (ModelUserStruct) TableName() string {
	return "users"
}
