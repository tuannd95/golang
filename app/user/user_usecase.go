package user

import (
	bUsecase "github.com/at-tuannguyen3/live-me-framework/usecase"
	"github.com/at-tuannguyen3/live-me-framework/utils"
	"github.com/jinzhu/gorm"
)

// UsecaseInterface interface.
type UsecaseInterface interface {
	getUsers() ([]ObjGetUserResponse, error)
}

// Usecase struct.
type Usecase struct {
	*bUsecase.BaseUsecase
	db         *gorm.DB
	repository RepositoryInterface
}

// getUsers func
func (u *Usecase) getUsers() ([]ObjGetUserResponse, error) {
	response := []ObjGetUserResponse{}
	users, err := u.repository.GetAll()
	if err != nil {
		return response, utils.ErrorsWrap(err, "repository.GetAll error")
	}

	for _, item := range users {
		user := ObjGetUserResponse{}
		user.Name = item.Name
		user.Email = item.Email
		response = append(response, user)
	}
	return response, nil
}

// NewUsecase func
func NewUsecase(logger *bUsecase.BaseUsecase, db *gorm.DB, r RepositoryInterface) *Usecase {
	return &Usecase{logger, db, r}
}
