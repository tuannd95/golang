package user

import (
	bRepository "github.com/at-tuannguyen3/live-me-framework/repository"
	bUtils "github.com/at-tuannguyen3/live-me-framework/utils"
	"github.com/jinzhu/gorm"
)

// RepositoryInterface interface.
type RepositoryInterface interface {
	GetAll() ([]ModelUserStruct, error)
}

// Repository struct.
type Repository struct {
	*bRepository.BaseRepository
	DB *gorm.DB
}

// GetAll func
func (r *Repository) GetAll() ([]ModelUserStruct, error) {
	result := []ModelUserStruct{}
	err := r.DB.Model(&ModelUserStruct{}).Select("name, email").Scan(&result).Error
	if err != nil {
		err = bUtils.ErrorsWrap(err, "can't find brand.")
	}
	return result, err
}

// NewRepository func
func NewRepository(br *bRepository.BaseRepository, db *gorm.DB) *Repository {
	return &Repository{br, db}
}
