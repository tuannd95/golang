package user

import (
	bResponse "github.com/at-tuannguyen3/live-me-framework/response"
)

// GetUserResponse struct
type GetUserResponse struct {
	bResponse.CommonResponse
	Result []ObjGetUserResponse `json:"result"`
}

// ObjGetUserResponse struct
type ObjGetUserResponse struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}
