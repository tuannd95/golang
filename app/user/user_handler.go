package user

import (
	"net/http"

	"github.com/at-tuannguyen3/live-me-framework/config/sql"
	bHandler "github.com/at-tuannguyen3/live-me-framework/handler"
	bRepository "github.com/at-tuannguyen3/live-me-framework/repository"
	bResponse "github.com/at-tuannguyen3/live-me-framework/response"
	bUsecase "github.com/at-tuannguyen3/live-me-framework/usecase"
	bUtils "github.com/at-tuannguyen3/live-me-framework/utils"
)

// HTTPHandler struct.
type HTTPHandler struct {
	*bHandler.BaseHTTPHandler
	usecase UsecaseInterface
}

// GetUsers func
func (h *HTTPHandler) GetUsers(w http.ResponseWriter, r *http.Request) {
	response := GetUserResponse{}
	results, err := h.usecase.getUsers()
	if err != nil {
		h.Logger.WithField(bUtils.APINameField, "getUsers").Info("h.usecase.getUsers() error", err)
		common := bResponse.CommonResponse{Message: "Internal Server Error", Errors: []string{}}
		h.ResponseJSON(w, http.StatusInternalServerError, common)
		return
	}
	commonResponse := bResponse.ResponseSuccess(http.StatusOK)
	response.CommonResponse = commonResponse
	response.Result = results
	h.ResponseJSON(w, http.StatusOK, response)

}

// NewHTTPHandler func
func NewHTTPHandler(bh *bHandler.BaseHTTPHandler, bu *bUsecase.BaseUsecase, br *bRepository.BaseRepository, db *sql.SQL) *HTTPHandler {
	r := NewRepository(br, db.DB)
	u := NewUsecase(bu, db.DB, r)
	return &HTTPHandler{bh, u}
}
