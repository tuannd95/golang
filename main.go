package main

import (
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/at-tuannguyen3/live-me-framework/config/sql"
	"github.com/at-tuannguyen3/live-me-framework/utils"
	"github.com/digital-fortress/live-me/router"
	env "github.com/digital-fortress/live-me/utils"
	"github.com/go-chi/chi"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	mux := chi.NewRouter()
	dbReadLogMode, _ := strconv.ParseBool(os.Getenv("DB_READ_LOG_MODE"))
	var dbInfo = sql.DBCONFIG{
		DBCONNECTTION: os.Getenv("DB_CONNECTION"),
		DBHOST:        os.Getenv("DB_HOST"),
		DBPORT:        os.Getenv("DB_PORT"),
		DBUSERNAME:    os.Getenv("DB_USERNAME"),
		DBPASSWORD:    os.Getenv("DB_PASSWORD"),
		DBDATABASE:    os.Getenv("DB_DATABASE"),
		LOGMODE:       dbReadLogMode,
	}
	// sql new.
	sqlHandler := sql.NewSQL(dbInfo)
	// logger new
	var loggerInfo = utils.LOGGERINFO{
		OUTPUT:  env.GetConfigString("logger.output"),
		LEVEL:   env.GetConfigString("logger.level"),
		FORMAT:  env.GetConfigString("logger.format"),
		LOGFILE: env.GetConfigString("logger.file"),
	}
	loggerHandle := utils.NewLogger(loggerInfo)
	r := &router.Router{
		Mux:           mux,
		SQLHandler:    sqlHandler,
		LoggerHandler: loggerHandle,
	}
	r.InitializeRouter()
	r.SetupHandler()
	http.ListenAndServe(":80", mux)
}
