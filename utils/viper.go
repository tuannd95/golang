package utils

import (
	"os"
	"strconv"

	"github.com/spf13/viper"
)

var (
	// ConfigPath is config path directory.
	ConfigPath = os.Getenv("GO_WORK_DIR") + "/config"
	// Environment is application environment.
	Environment = os.Getenv("ENVIRONMENT")
)

// init sets viper configuration.
func init() {
	viper.AddConfigPath(ConfigPath) // path to look for the config file in
	viper.SetConfigName(Environment)
	viper.SetConfigType("json")
	viper.AutomaticEnv()
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {
		panic(err)
	}

	if viper.MergeInConfig() != nil {
		panic(err)
	}

	viper.WatchConfig()
}

// SetConfig set value to config file.
func SetConfig(key string, value interface{}) {
	viper.Set(key, value)
}

// GetConfigString get string from config file.
func GetConfigString(key string) string {
	return viper.GetString(key)
}

// GetConfigInt get int from config file.
func GetConfigInt(key string) int {
	return viper.GetInt(key)
}

// GetConfigInt64 get int64 from config file.
func GetConfigInt64(key string) int64 {
	return viper.GetInt64(key)
}

// GetConfigBool get bool from config file.
func GetConfigBool(key string) bool {
	return viper.GetBool(key)
}

// GetConfigByte get []byte from config file.
func GetConfigByte(key string) []byte {
	return []byte(viper.GetString(key))
}

// GetConfigStringSlice []string from config file.
func GetConfigStringSlice(key string) []string {
	return viper.GetStringSlice(key)
}

// GetenvBool from env variable.
func GetenvBool(key string) bool {
	b, _ := strconv.ParseBool(os.Getenv(key))
	return b
}

// GetenvInt64 from env variable.
func GetenvInt64(key string) int64 {
	i, _ := strconv.ParseInt(os.Getenv(key), 10, 32)
	return i
}

// GetenvBytes from env variable.
func GetenvBytes(key string) []byte {
	return []byte(os.Getenv(key))
}
