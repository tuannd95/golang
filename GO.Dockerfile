FROM golang:1.9-alpine

COPY entrypoint.sh /
COPY Gopkg.toml /
RUN apk add --no-cache ca-certificates \
        dpkg \
        gcc \
        git \
        musl-dev \
        bash

ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" \
    && chmod -R 777 "$GOPATH" \
    && chmod +x /entrypoint.sh

RUN go get github.com/tockins/realize
RUN go get -u github.com/golang/dep/cmd/dep
RUN go get -u bitbucket.org/liamstask/goose/cmd/goose

RUN mkdir -p /var/log/go && \
    ln -sf /dev/stdout /var/log/go/digital-api.log

WORKDIR /go/src/github.com/digital-fortress/live-me
ENTRYPOINT ["/entrypoint.sh"]

CMD ["realize", "start"]
